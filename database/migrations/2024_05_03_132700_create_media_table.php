<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->boolean('serial');
            $table->string('name');
            $table->string('original_name')->nullable();
            $table->string('slug')->unique();
            $table->string('farsi_name')->nullable();
            $table->string('label')->nullable();
            $table->float('imdb_rate')->nullable();
            $table->float('limo_rate')->nullable();
            $table->string('imdb_sku')->nullable();
            $table->boolean('active');
            $table->string('cover')->nullable();
            $table->string('second_cover')->nullable();
            $table->string('trailer')->nullable();
            $table->boolean('main_lang')->default(true);
            $table->boolean('subtitle')->default(true);
            $table->boolean('dubbed')->default(false);
            $table->text('description')->nullable();
            $table->text('summery')->nullable();
            $table->string('age_certification')->nullable();
            $table->string('release_date');
            $table->string('duration')->nullable();
            $table->integer('view')->default(0);
//            $table->foreignIdFor(\App\Models\Country::class)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('media');
    }
};

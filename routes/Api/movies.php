<?php

use App\Http\Controllers\SourceController;
use Illuminate\Support\Facades\Route;


Route::prefix('/api/v1/')->group(function () {
    Route::get('slider', [\App\Http\Controllers\SliderController::class, 'list']);

});

Route::prefix('/api/v1/movie')->group(function () {
    Route::post('/create', [\App\Http\Controllers\MovieController::class, 'create']);
    Route::get('/list', [\App\Http\Controllers\MovieController::class, 'list']);

    Route::prefix('/source')->group(function () {

        Route::post('/add', [SourceController::class, 'addMovieSource']);
        Route::get('/{slug}/list', [SourceController::class, 'showMovieSource']);
        Route::get('/{id}/delete', [SourceController::class, 'deleteMovieSource']);
        Route::get('/{id}/show' , [SourceController::class, 'showSource']);
        Route::post('/{id}/edit' , [SourceController::class, 'editSource']);
    });

});


Route::prefix('/api/v1/serial')->group(function () {
    Route::post('movie/create', [\App\Http\Controllers\MovieController::class, 'create']);
    Route::get('/list', [\App\Http\Controllers\SerialController::class, 'list']);
});

Route::prefix('/api/v1/media')->group(function () {
    Route::get('/{slug}', [\App\Http\Controllers\MovieController::class, 'show']);
});

<?php

namespace App\Http\Controllers;

use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MovieController extends Controller
{
    public function create(Request $request)
    {


        if (Media::where('slug', $request->slug)->count()) {
            return response()->json(['success' => false, 'error' => 'پیوند یکتا قبلا استفاده شده!']);
        }
        $media = Media::create([
            'serial' => 0,
            'name' => $request->englishName,
            'farsi_name' => $request->name,
            'original_name' => $request->originalName,
            'label' => $request->label,
            'active' => 0,
            'release_date' => $request->release_date,
            'slug' => $request->slug,
            'imdb_rate' => request()->imdb_rate,
            'limo_rate' => request()->limo_rate,
            'imdb_sku' => request()->imdb_sku,
            'age_certification' => request()->age_certification,
            'duration' => request()->duration,
        ]);

        if ($request->hasFile('main_cover')) {
            $image = $request->file('main_cover');
            $image_path = Storage::disk('public')->put('images', $image);
            $media->cover = $image_path;
            $media->save();
        }
        if ($request->hasFile('wide_cover')) {
            $image = $request->file('wide_cover');
            $image_path = Storage::disk('public')->put('images', $image);
            $media->second_cover = $image_path;
            $media->save();
        }


        return response()->json(['success' => true]);
    }

    public function list(Request $request)
    {
        $media = Media::where('serial', 0)->get();

        return response()->json(['success' => true, 'data' => $media]);

    }


    public function show(string $slug)
    {
        $media = Media::where('slug', $slug)->firstOrFail();
        return response()->json(['success' => true, 'data' => $media]);
    }

}

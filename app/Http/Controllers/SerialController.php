<?php

namespace App\Http\Controllers;

use App\Models\Media;
use Illuminate\Http\Request;

class SerialController extends Controller
{
    public function list(Request $request)
    {
        $serials = Media::where('serial' , 1)->get();
        return response()->json(['success' => true , 'data' => $serials]);
    }
}

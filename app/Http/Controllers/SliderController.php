<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
     public function list()
     {
         $sliders = Slider::all();
         $sliders->load('media');

         return response()->json(['success' => true, 'data' => $sliders]);


     }
}

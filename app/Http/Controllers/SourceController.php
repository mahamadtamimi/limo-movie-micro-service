<?php

namespace App\Http\Controllers;

use App\Models\Media;
use App\Models\Source;
use Illuminate\Http\Request;

class SourceController extends Controller
{
    public function addMovieSource(Request $request)
    {

        $media = Media::where('slug', $request->slug);
        if ($media->count() > 0) {

            $source = new Source([
                'quality' => $request->quality,
                'lang' => $request->lanq,
                'size' => $request->size,
                'src' => $request->source,
            ]);

            $media = Media::where('slug', $request->slug)->first()->sources()->save($source);
            return response()->json(['success' => true]);
        }


        return response()->json(['success' => false]);
    }

    public function showMovieSource(string $slug)
    {
        $media = Media::where('slug', $slug)->first();
        $media->load('sources');
        return response()->json(['success' => true, 'data' => $media->sources]);
    }

    public function deleteMovieSource(string $id)
    {
        $media = Source::where('id', intval($id))->delete();
        return response()->json(['success' => true , 'data' => $id]);
    }

    public function showSource(string $id){
        $source = Source::where('id', intval($id))->first();
        return response()->json(['success' => true, 'data' => $source]);

    }


    public function editSource(Request $request , string $id){
        $source = Source::where('id', intval($id))->first();
        $source->quality = $request->quality;
        $source->src = $request->source;
        $source->size = $request->size;
        $source->save();
        return response()->json(['success' => true ]);
    }
}
